/**
 * PostController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


  /**
   * `PostController.add()`
   */
  add: async function (req, res) {
    const foundUser = await User.findOne({token: req.headers.token});

    if (!foundUser) return res.forbidden();

    const createdPost = await Post.create({
        message: req.body.message,
        author: foundUser.id,
    })
    .fetch()
    .then((post) => Post.findOne({id: post.id}).populate('author'));

    return res.json(Post.toResJson(createdPost));
  },

  /**
   * `PostController.get()`
   */
  get: async function (req, res) {
    const foundPost = await Post.findOne({id: req.params.id})
        .populate('author');


      return res.json(Post.toResJson(foundPost));
  },

  /**
   * `PostController.edit()`
   */
  edit: async function (req, res) {
    const updatedPost = await Post.update({id: req.params.id})
        .set({
            message: req.body.message
        })
        .fetch()
        .then((post) => Post.findOne({id: post[0].id}).populate('author'));


      return res.json(Post.toResJson(updatedPost));
  },

  /**
   * `PostController.delete()`
   */
  delete: async function (req, res) {
    await Post.destroy({id: req.params.id});

    return res.ok();
  },

  /**
   * `PostController.show()`
   */
  list: async function (req, res) {
    const listPosts = await Post.find()
        .populate('author')
        .then( (results) => results.map((post) => {return Post.toResJson(post)}));

    return res.json({
        posts: listPosts
    });
  }

};

