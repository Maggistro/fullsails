/**
 * CommentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


    /**
     * `CommentController.add()`
     */
    add: async function (req, res) {
        const createdComment = await Comment.create({
            postId: req.params.postId,
            message: req.body.message,
        })
            .fetch()

        return res.json(Comment.toResJson(createdComment));
    },

    /**
     * `CommentController.get()`
     */
    get: async function (req, res) {
        const foundComment = await Comment.findOne({id: req.params.id});

        return res.json(Comment.toResJson(foundComment));
    },

    /**
     * `CommentController.edit()`
     */
    edit: async function (req, res) {
        const updatedComment = await Comment.update({id: req.params.id})
            .set({message: req.body.message})
            .fetch()
            .then( (results) => results[0]);

        return res.json(Comment.toResJson(updatedComment));
    },

    /**
     * `CommentController.delete()`
     */
    delete: async function (req, res) {
        await Comment.destroy({id: req.params.id})

        return res.ok();
    },

    /**
     * `CommentController.list()`
     */
    list: async function (req, res) {
        const listComment = await Comment.find({postId: req.params.postId})
            .then( (results) => results.map((comment) => {return Comment.toResJson(comment)}));

        return res.json({
            postId: req.params.postId,
            comments: listComment
        })
    }

};

