const uuid = require('uuid');

/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


    /**
     * `UserController.index()`
     */
    list: async function (req, res) {
        const users = await User.find({
            where: {},
            select: ['name', 'email']
        })

        return res.json({
            users: users
        })
    },

    /**
     * `UserController.create()`
     */
    add: async function (req, res) {

        const token = uuid();

        const createdUser = await User.create({
            name: req.body.name,
            email: req.body.email,
            token: token
        }).fetch();

        return res.json({
            id: createdUser.id,
            token: createdUser.token,
            name: createdUser.name,
            email: createdUser.email
        });
    },

    /**
     * `UserController.show()`
     */
    get: async function (req, res) {
        const foundUser = await User.findOne({
              id: req.params.id
        });

        return res.json({
            id: foundUser.id,
            name: foundUser.name,
            email: foundUser.email
        });
    },

    /**
     * `UserController.edit()`
     */
    edit: async function (req, res) {
        const updateUser = await User
            .update({id: req.params.id})
            .set({
                name: req.body.name,
                email: req.body.email
            })
            .fetch()
            .then( (users) => users[0]);

        return res.json({
            id: updateUser.id,
            name: updateUser.name,
            email: updateUser.email
        });
    },

    /**
     * `UserController.delete()`
     */
    delete: async function (req, res) {
        await User.destroy({id: req.params.id});

        return res.ok();
    }

};

