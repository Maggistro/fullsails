/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  

  /**
   * `TestController.list()`
   */
  list: async function (req, res) {
    return res.json({
      todo: 'list() is not implemented yet!'
    });
  },

  /**
   * `TestController.create()`
   */
  create: async function (req, res) {
    return res.json({
      todo: 'create() is not implemented yet!'
    });
  }

};

