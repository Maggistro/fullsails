CREATE USER sails with PASSWORD 'fullsailahead';
CREATE DATABASE fullsails;
GRANT ALL PRIVILEGES ON DATABASE fullsails TO sails;